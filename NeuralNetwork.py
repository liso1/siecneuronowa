import numpy as np
import math
from copy import deepcopy
import random
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import csv

class NeuralNetwork:
    def __init__(self, layers, activation_function='LINEAR'):
        """
        :param layers: oznacza wszystkie warsty, np [8,4,17] oznacza 8 wejsc, 4 neurony w warstwie ukrytej i 17 wyjsc, zaś [2,1] oznacza dwa wejscia i jedno wyjscie bez warstwy ukrytej.
        :param activation_function: LINEAR albo SIGMOID
        """
        '''UWAGA!!! Jeśli występuje, to siec automatycznie przełącza się na SIGMOIDY! Jest
        to spowodowane tym, że już przy jednej warstwie ukrytej neurony przekraczały dopuszczalne wartości floatów i
        osiągały NAN'''

        self.__activation_type = activation_function if len(layers)<3 else 'SIGMOID'
        self.__layers=layers

        '''Utwórz różne rodzaje funkcji aktywacji'''
        self.__activation_functions = {
            'LINEAR': self.__linearActivation,
            'SIGMOID': self.__sigmoidActivation
        }

        '''pochodne funkcji aktywacji'''
        self.__activation_derivatives= {
            'LINEAR': self.__linearDerivative,
            'SIGMOID': self.__sigmoidDerivative
        }
        self.__errors=[]
        self.__accuracies = []
        self.__validation_errors=[]
        self.__validation_accuracies = []

    def setData(self, data, labels):
        """
        :param data: lista z wektorami wejsc, zerowy element wektora powinien byc zawsze jedynka
        :param labels: etykietki przypisane do kazdego z wektorów wejsciowych
        """
        self.__data = data
        self.__labels=labels
        self.__validation_data=[]
        self.__validation_labels = []

    def getErrors(self):
        return self.__errors

    def getAccuracies(self):
        return self.__accuracies

    def initializeNet(self):
        '''Ustawia losowe wagi, biasy, zeruje błędy
            Wektor weights: [a][b][c]: a - numer warstwy (przy czym zero oznacza pierwszą warstwę ukrytą lub wyjściowa, gdy brak ukrytej;
            b - indeks neuronu w warstwie, c - indeks wagi
            Wektor biases: [a][b], a - nr warstwy, b - nr neuronu, dla ktorego obowiązuje dany bias'''
        self.__errors = []
        self.__accuracies = []
        self.__validation_errors = []
        self.__validation_accuracies = []
        self.__weights = [np.random.randn(y, x) for x, y in zip(self.__layers[:-1], self.__layers[1:])]
        self.__biases = [np.random.randn(y, 1) for y in self.__layers[1:]]
        self.__historical_weights=[deepcopy([np.random.randn(y, x) for x, y in zip(self.__layers[:-1], self.__layers[1:])])]

    def __linearActivation(self,PSP):
        return PSP

    def __sigmoidActivation(self, PSP):
        return 1.0 / (1.0 + np.exp(-PSP))

    def __linearDerivative(self,y):
        return 1

    def __sigmoidDerivative(self, y):
        return y*(1-y)

    def feedforward(self, input, already_transposed=False):
        if already_transposed==False:
            input = input.T
        self.__activations=[]
        for biases, weights in zip(self.__biases, self.__weights):
            input = self.__activation_functions[self.__activation_type](np.dot(weights, input) + biases)#sigmoid(np.dot(w, a) + b)
            self.__activations.append(input)
        return input

    def backpropagate(self, labels, already_transposed=False):
        if already_transposed == False:
            labels = labels.T
        self.__deltas=[labels-self.__activations[-1]]
        for layer_number in reversed(range(len(self.__activations)-1)):
            layer_deltas=[]
            for neuron_number in range(self.__layers[layer_number+1]):
                neuron_delta=0
                for next_neuron_number in range(self.__layers[layer_number+2]):
                    neuron_delta+=(self.__weights[layer_number+1][next_neuron_number][neuron_number])*self.__deltas[-1][next_neuron_number]
                layer_deltas.append(neuron_delta)
            self.__deltas.append(layer_deltas)



    def update_weights(self, input, eta, already_transposed=False):
        if already_transposed == False:
            input = input.T
        input_layers=[input[i][0] for i in range(len(input))]
        input_layers=[input_layers]+list(self.__activations)
        self.__deltas=list(reversed(self.__deltas))
        for layer_number in range(len(self.__activations)):
            for neuron_number in range(self.__layers[layer_number+1]):
                for prev_neuron_number in range(self.__layers[layer_number]):
                    self.__weights[layer_number][neuron_number][prev_neuron_number]+=eta*self.__deltas[layer_number][neuron_number]*self.__activation_derivatives[self.__activation_type](self.__activations[layer_number][neuron_number])*input_layers[layer_number][prev_neuron_number]


    def getActivations(self):
        return self.__activations
    def getWeights(self):
        return self.__weights
    def getDeltas(self):
        return self.__deltas

    def trainStep(self, input, label, eta=0.1):
        input=input.T
        label = label.T
        """
        Uczenie pojedynczego
        :param input: pojedynczy przyklad
        :param label: pojedyncza etykietka dla przykladu
        :param eta: parametr szybkosci uczenia
        """
        self.feedforward(input, already_transposed=True)
        if len(self.__activations[-1]) != len(label):
            print("Rozmiary wyjścia i etykieta są różne!")
            return
        else:
            self.backpropagate(label, already_transposed=True)
            self.update_weights(input, eta, already_transposed=True)

    def calculateError(self, inputs, labels):
        data_len = len(inputs)
        if data_len != len(labels):
            print("Rozmiary wyjścia i etykieta są różne!")
            return
        # wymeiszaj dane na wszelki wypadek
        inputs, labels = shuffle(inputs, labels)
        predicted_outputs=[]
        for input, label in zip(inputs, labels):
            predicted_outputs.append(self.predictOutput(input))

        return np.mean(np.square(np.subtract(labels, predicted_outputs)))

    def calculateAccuracy(self, inputs, labels):
        data_len = len(inputs)
        if data_len != len(labels):
            print("Rozmiary wyjścia i etykieta są różne!")
            return
        # wymeiszaj dane na wszelki wypadek
        inputs, labels = shuffle(inputs, labels)
        predicted_outputs = []
        for input, label in zip(inputs, labels):
            predicted_outputs.append(self.predictOutput(input, binarize_output=True))
        matches = np.sum(int(np.array_equal(predicted_outputs[i], labels[i])) for i in range(data_len))
        return matches/data_len


    def trainBatch(self, inputs, labels, eta=0.1):
        if len(inputs)!=len(labels):
            print("Rozmiary wyjścia i etykieta są różne!")
            return
        else:
            for input, label in zip(inputs, labels):
                self.trainStep(input, label, eta)

    def trainEpoch(self, inputs, labels, eta=0.1, batch_size=0):
        """
        Trenuje całą epokę, na całym zbiorze
        :param inputs: zbiór przykładów
        :param labels: zbiór etykiet
        :param batch_size: rozmiar batcha. Jeśli batch ma mieć rozmiar całego datasetu, to batch_size=0
        """
        data_len = len(inputs)
        if data_len!=len(labels):
            print("Rozmiary wyjścia i etykieta są różne!")
            return
        #wymieszaj dane co epokę
        inputs, labels = shuffle(inputs, labels)
        if batch_size>0:
            #gdy batch_size jes wiekszy od datasetu, to ogranicz jego rozmiar do rozmiaru datasetu
            if batch_size>data_len:
                batch_size=data_len

            batch_count=math.floor(data_len/batch_size)
            if data_len%batch_size>0:
                batch_count+=1

            for i in range(batch_count):
                data_batch=inputs[i*batch_size:(i+1)*batch_size]
                label_batch=labels[i * batch_size:(i + 1) * batch_size]
                self.trainBatch(data_batch, label_batch, eta)

        else:
            self.trainBatch(inputs, labels, eta)
        self.__historical_weights.append(deepcopy((self.__weights)))

    def trainNet(self, epochs, eta=0.1, print_errors=False, print_accuracy=False, batch_size=0):
        for i in range(epochs):
            self.trainEpoch(self.__data, self.__labels, eta=eta, batch_size=batch_size)
            error = self.calculateError(self.__data, self.__labels)
            accuracy = self.calculateAccuracy(self.__data, self.__labels)
            self.__errors.append(error)
            self.__accuracies.append(accuracy)
            if (self.__validation_data and self.__validation_labels):
                validation_error = self.calculateError(self.__validation_data, self.__validation_labels)
                validation_accuracy= self.calculateAccuracy(self.__validation_data, self.__validation_labels)
                self.__validation_errors.append(validation_error)
                self.__validation_accuracies.append(validation_accuracy)

            if print_errors==True:
                print("EPOKA:", i+1, "Blad: ", error)
            if print_accuracy == True:
                print("EPOKA:", i+1, "Trafność: ", accuracy)

    def drawError(self):
        plt.figure()
        plt.plot(self.__errors, label="Zbiór uczący")
        plt.ylabel('Błąd średniokwadratowy')
        plt.xlabel('Epoka')
        plt.title(self.__activation_type +" "+  '-'.join(str(x) for x in self.__layers))
        if self.__validation_errors:
            plt.plot(self.__validation_errors, label="Zbiór testowy")
        plt.legend(loc='upper right')

    def drawAccuracy(self):
        plt.figure()
        plt.plot(self.__accuracies, label="Zbiór uczący")
        plt.ylabel('Średnia trafność')
        plt.xlabel('Epoka')
        plt.title(self.__activation_type + " " + '-'.join(str(x) for x in self.__layers))
        if self.__validation_accuracies:
            plt.plot(self.__validation_accuracies, label="Zbiór testowy")
        plt.legend(loc='lower right')

    def predictOutput(self, input, force_single_output=False, binarize_output=False):
        self.feedforward(input)
        if force_single_output==True:
            single_output=np.zeros(self.__activations[-1].shape)
            single_output[np.argmax(self.__activations[-1])]=1
            return [[single_output[i][0] for i in range(len(single_output))]]
        elif binarize_output==True:
            return [[np.rint(self.__activations[-1][i][0]) for i in range(len(self.__activations[-1]))]]
        else:
            return [[self.__activations[-1][i][0]  for i in range(len(self.__activations[-1]))]]

    def drawWeights(self, layer_number, neuron_number, previous_neuron_number):
        """
        # [a][b][c][d]
        # a - nr epoki
        # b - nr warstwy
        # c - nr neuronu w warstwie
        # d - nr neuronu w poprzedniej warstwie
        :param layer_number: numer warstwy. Uwaga! Jest o jeden mniej warstw wag, niz neuronów, wiec tak naprawdę layer = 1 oznacza warstwe wag 0!

        """
        if layer_number<1:
            print("Nie można zobaczyć wag z warstwy mniejszej niż 1!")
            return
        weights=[self.__historical_weights[i][layer_number-1][neuron_number][previous_neuron_number] for i in range(len(self.__historical_weights))]
        plt.figure()
        plt.plot(weights)
        plt.ylabel('Waga')
        plt.xlabel('Epoka')
        plt.title("Warstwa: " + str(layer_number) + ", neuron nr: "+str(neuron_number)+", nr neuronu z warstwy "+str(layer_number-1)+": "+str(previous_neuron_number) )

    def drawAllCharts(self):
        plt.show()

    def loadCsv(self, path, convertLabelsToFloat=True):
        """
        Funkcja zwraca dane i etykietki. Zaklada, że ostatnia kolumna
        pliku CSV to etykietka i że nie ma brakujących danych
        :param path: ścieżka do pliku
        :return:
        """
        data=[]
        labels=[]
        with open(path) as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                data_row=[float(row[i]) for i in range(len(row[:-1]))]
                data.append(data_row)
                if convertLabelsToFloat==True:
                    labels.append([float(row[-1])])
                else:
                    labels.append([row[-1]])
            data=np.array(data)
            labels = np.array(labels)
            return data, labels

    def normalizeDataValues(self, data):
        """
        Funkcja normalizuje wartosci datasetu lub labeli
        :param data: dane uczące lub etykietki
        """
        array_data=np.array(data)
        row_len=len(array_data[0])
        maximal_values=[np.max(array_data[:, i]) for i in range(row_len)]
        for i in range(row_len):
            data[:, i]/=maximal_values[i]
        return data

    def convertDataToNetReadable(self, data):
        """
        Potrzebne, by skonwertować zwykle tablice na dane w odpowiedniej postaci, na przyklad:
        inputs=np.array([[[0,0,1]], [[1,0,1]], [[1,1,0]], [[1,1,1]]])
        labels=np.array([[[0,1]], [[1,0]], [[0,0]],[[0,1]] ]),
        bo taki format jest potrzebny w sieci
        :param data: dane uczące lub etykietki
        """
        data_len = len(data)
        converted_data=np.array([[data[i]] for i in range(data_len)])
        return converted_data


    def loadPima(self): #uwzględnij scieżke w inotebooku
        #pima nie wymaga normalizacji wyjsc
        raw_data, raw_labels = self.loadCsv("pima.csv")
        normalized_data = self.normalizeDataValues(raw_data)
        net_readable_data = self.convertDataToNetReadable(normalized_data)
        net_readable_labels = self.convertDataToNetReadable(raw_labels)
        self.setData(net_readable_data, net_readable_labels)

    def createValidationSet(self, dataRatio=0.3):
        self.__validation_data=[]
        self.__validation_labels = []
        if dataRatio>0 and dataRatio<1:
            validation_lenght=int(dataRatio*len(self.__data))
            for k in range(validation_lenght):
                i=random.randint(0, len(self.__data)-1)
                self.__validation_data.append(self.__data[i])
                self.__validation_labels.append(self.__labels[i])
                self.__data = np.delete(self.__data, i, axis=0)
                self.__labels = np.delete(self.__labels, i, axis=0)


    def loadBupa(self): #uwzględnij scieżke w inotebooku
        raw_data, raw_labels = self.loadCsv("bupa.csv")
        normalized_data = self.normalizeDataValues(raw_data)
        for label in raw_labels:
            label-=1
        net_readable_data = self.convertDataToNetReadable(normalized_data)
        net_readable_labels = self.convertDataToNetReadable(raw_labels)
        self.setData(net_readable_data, net_readable_labels)


    def loadIris(self):
        raw_data, raw_labels = self.loadCsv("iris.csv", convertLabelsToFloat=False)
        normalized_data = self.normalizeDataValues(raw_data)
        labels=[]
        for label in raw_labels:
            if label=='Iris-setosa':
                labels.append([1, 0, 0])
            elif label=='Iris-versicolor':
                labels.append([0,1, 0])
            elif label == 'Iris-virginica':
                labels.append([0,0,1])
        net_readable_data = self.convertDataToNetReadable(normalized_data)
        net_readable_labels = self.convertDataToNetReadable(labels)
        self.setData(net_readable_data, net_readable_labels)


